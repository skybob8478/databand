package org.databandtech.api.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 批处理计划对象 databand_scheduletask
 * 
 * @author databand
 * @date 2020-12-31
 */
public class DatabandScheduletask
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 任务类型 */
    private String jobtype;

    /** 任务关键key，任务实例id */
    private String jobcode;

    /** bean名称，只在java型任务有效 */
    private String beaname;

    /** 方法名称，只在java型任务有效 */
    private String methodname;

    /** 方法参数，只在java型任务有效 */
    private String methodparams;

    /** 任务描述 */
    private String descri;

    /** cron任务表达式 */
    private String cron;

    /** 扩展字段 */
    private String ext;

    /** 成功 1 是 0 否 */
    private Integer status;

    /** $column.columnComment */
    private Integer startflag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setJobtype(String jobtype) 
    {
        this.jobtype = jobtype;
    }

    public String getJobtype() 
    {
        return jobtype;
    }
    public void setJobcode(String jobcode) 
    {
        this.jobcode = jobcode;
    }

    public String getJobcode() 
    {
        return jobcode;
    }
    public void setBeaname(String beaname) 
    {
        this.beaname = beaname;
    }

    public String getBeaname() 
    {
        return beaname;
    }
    public void setMethodname(String methodname) 
    {
        this.methodname = methodname;
    }

    public String getMethodname() 
    {
        return methodname;
    }
    public void setMethodparams(String methodparams) 
    {
        this.methodparams = methodparams;
    }

    public String getMethodparams() 
    {
        return methodparams;
    }
    public void setDescri(String descri) 
    {
        this.descri = descri;
    }

    public String getDescri() 
    {
        return descri;
    }
    public void setCron(String cron) 
    {
        this.cron = cron;
    }

    public String getCron() 
    {
        return cron;
    }
    public void setExt(String ext) 
    {
        this.ext = ext;
    }

    public String getExt() 
    {
        return ext;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setStartflag(Integer startflag) 
    {
        this.startflag = startflag;
    }

    public Integer getStartflag() 
    {
        return startflag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jobtype", getJobtype())
            .append("jobcode", getJobcode())
            .append("beaname", getBeaname())
            .append("methodname", getMethodname())
            .append("methodparams", getMethodparams())
            .append("descri", getDescri())
            .append("cron", getCron())
            .append("ext", getExt())
            .append("status", getStatus())
            .append("startflag", getStartflag())
            .toString();
    }
}
