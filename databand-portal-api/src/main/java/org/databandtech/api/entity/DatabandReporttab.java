package org.databandtech.api.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 报页对象 databand_reporttab
 * 
 * @author databand
 * @date 2020-12-31
 */
public class DatabandReporttab 
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 所属报表 */
    private Long reportid;
    
    DatabandReport report;
    private String title;
    private String sql;
    private String listsql;
    private String apiurl;
    private String apiparam;
    private String apitag;
    private Long sortnum;
    private Long sourceid;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setReportid(Long reportid) 
    {
        this.reportid = reportid;
    }

    public Long getReportid() 
    {
        return reportid;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSql(String sql) 
    {
        this.sql = sql;
    }

    public String getSql() 
    {
        return sql;
    }
    public void setListsql(String listsql) 
    {
        this.listsql = listsql;
    }

    public String getListsql() 
    {
        return listsql;
    }
    public void setSortnum(Long sortnum) 
    {
        this.sortnum = sortnum;
    }

    public Long getSortnum() 
    {
        return sortnum;
    }
    public void setSourceid(Long sourceid) 
    {
        this.sourceid = sourceid;
    }

    public Long getSourceid() 
    {
        return sourceid;
    }
    
    public DatabandReport getReport() {
        if (report == null)
        {
        	report = new DatabandReport();
        }
        return report;
	}

	public void setReport(DatabandReport report) {
		this.report = report;
	}
	

    public String getApiurl() {
		return apiurl;
	}

	public void setApiurl(String apiurl) {
		this.apiurl = apiurl;
	}

	public String getApiparam() {
		return apiparam;
	}

	public void setApiparam(String apiparam) {
		this.apiparam = apiparam;
	}

	public String getApitag() {
		return apitag;
	}

	public void setApitag(String apitag) {
		this.apitag = apitag;
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("reportid", getReportid())
            .append("title", getTitle())
            .append("sql", getSql())
            .append("listsql", getListsql())
            .append("sortnum", getSortnum())
            .append("sourceid", getSourceid())
            .toString();
    }
}
