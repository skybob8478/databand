package org.databandtech.api.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 站点菜单对象 databand_sitemenu
 * 
 * @author databand
 * @date 2020-12-31
 */
public class DatabandSitemenu 
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 站点id */
    private Long siteid;

    /** 站点菜单 */
    private String menuname;

    /** 排序 */
    private Long sort;

    /** 父栏目 */
    private Long parentid;

    /** 关联报表 */
    private Long reportid;

    /** 图标 */
    private String icon;

    /** 节点类型,1:根；2：菜单；3：叶子，可关联报表 */
    private Integer nodetype;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSiteid(Long siteid) 
    {
        this.siteid = siteid;
    }

    public Long getSiteid() 
    {
        return siteid;
    }
    public void setMenuname(String menuname) 
    {
        this.menuname = menuname;
    }

    public String getMenuname() 
    {
        return menuname;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setParentid(Long parentid) 
    {
        this.parentid = parentid;
    }

    public Long getParentid() 
    {
        return parentid;
    }
    public void setReportid(Long reportid) 
    {
        this.reportid = reportid;
    }

    public Long getReportid() 
    {
        return reportid;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setNodetype(Integer nodetype) 
    {
        this.nodetype = nodetype;
    }

    public Integer getNodetype() 
    {
        return nodetype;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("siteid", getSiteid())
            .append("menuname", getMenuname())
            .append("sort", getSort())
            .append("parentid", getParentid())
            .append("reportid", getReportid())
            .append("icon", getIcon())
            .append("nodetype", getNodetype())
            .toString();
    }
}
