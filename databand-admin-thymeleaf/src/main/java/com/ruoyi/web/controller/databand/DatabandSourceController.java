package com.ruoyi.web.controller.databand;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.DatabandSource;
import com.ruoyi.web.service.IDatabandSourceService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据源Controller
 * 
 * @author databand
 * @date 2020-12-30
 */
@Controller
@RequestMapping("/web/source")
public class DatabandSourceController extends BaseController
{
    private String prefix = "web/source";

    @Autowired
    private IDatabandSourceService databandSourceService;
    
	@Qualifier(value = "dataSourceDrives")
	@Autowired
	private Map<String, String> dataSourceDrives;

    @RequiresPermissions("web:source:view")
    @GetMapping()
    public String source()
    {
        return prefix + "/source";
    }

    /**
     * 查询数据源列表
     */
    @RequiresPermissions("web:source:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DatabandSource databandSource)
    {
        startPage();
        List<DatabandSource> list = databandSourceService.selectDatabandSourceList(databandSource);
        return getDataTable(list);
    }

    /**
     * 导出数据源列表
     */
    @RequiresPermissions("web:source:export")
    @Log(title = "数据源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DatabandSource databandSource)
    {
        List<DatabandSource> list = databandSourceService.selectDatabandSourceList(databandSource);
        ExcelUtil<DatabandSource> util = new ExcelUtil<DatabandSource>(DatabandSource.class);
        return util.exportExcel(list, "source");
    }

    /**
     * 新增数据源
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("dataSourceDrives", dataSourceDrives);
        return prefix + "/add";
    }

    /**
     * 新增保存数据源
     */
    @RequiresPermissions("web:source:add")
    @Log(title = "数据源", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DatabandSource databandSource)
    {
        return toAjax(databandSourceService.insertDatabandSource(databandSource));
    }

    /**
     * 修改数据源
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DatabandSource databandSource = databandSourceService.selectDatabandSourceById(id);
        mmap.put("databandSource", databandSource);
        mmap.put("dataSourceDrives", dataSourceDrives);
        return prefix + "/edit";
    }

    /**
     * 修改保存数据源
     */
    @RequiresPermissions("web:source:edit")
    @Log(title = "数据源", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DatabandSource databandSource)
    {
        return toAjax(databandSourceService.updateDatabandSource(databandSource));
    }

    /**
     * 删除数据源
     */
    @RequiresPermissions("web:source:remove")
    @Log(title = "数据源", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(databandSourceService.deleteDatabandSourceByIds(ids));
    }
}
