package com.ruoyi.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.web.mapper.DatabandReporthistroyMapper;
import com.ruoyi.web.domain.DatabandReporthistroy;
import com.ruoyi.web.service.IDatabandReporthistroyService;
import com.ruoyi.common.core.text.Convert;

/**
 * 报版本Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandReporthistroyServiceImpl implements IDatabandReporthistroyService 
{
    @Autowired
    private DatabandReporthistroyMapper databandReporthistroyMapper;

    /**
     * 查询报版本
     * 
     * @param id 报版本ID
     * @return 报版本
     */
    @Override
    public DatabandReporthistroy selectDatabandReporthistroyById(Long id)
    {
        return databandReporthistroyMapper.selectDatabandReporthistroyById(id);
    }

    /**
     * 查询报版本列表
     * 
     * @param databandReporthistroy 报版本
     * @return 报版本
     */
    @Override
    public List<DatabandReporthistroy> selectDatabandReporthistroyList(DatabandReporthistroy databandReporthistroy)
    {
        return databandReporthistroyMapper.selectDatabandReporthistroyList(databandReporthistroy);
    }

    /**
     * 新增报版本
     * 
     * @param databandReporthistroy 报版本
     * @return 结果
     */
    @Override
    public int insertDatabandReporthistroy(DatabandReporthistroy databandReporthistroy)
    {
        return databandReporthistroyMapper.insertDatabandReporthistroy(databandReporthistroy);
    }

    /**
     * 修改报版本
     * 
     * @param databandReporthistroy 报版本
     * @return 结果
     */
    @Override
    public int updateDatabandReporthistroy(DatabandReporthistroy databandReporthistroy)
    {
        return databandReporthistroyMapper.updateDatabandReporthistroy(databandReporthistroy);
    }

    /**
     * 删除报版本对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDatabandReporthistroyByIds(String ids)
    {
        return databandReporthistroyMapper.deleteDatabandReporthistroyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除报版本信息
     * 
     * @param id 报版本ID
     * @return 结果
     */
    @Override
    public int deleteDatabandReporthistroyById(Long id)
    {
        return databandReporthistroyMapper.deleteDatabandReporthistroyById(id);
    }
}
