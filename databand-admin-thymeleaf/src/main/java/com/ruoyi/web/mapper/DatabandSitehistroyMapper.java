package com.ruoyi.web.mapper;

import java.util.List;
import com.ruoyi.web.domain.DatabandSitehistroy;

/**
 * 站点历史Mapper接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface DatabandSitehistroyMapper 
{
    /**
     * 查询站点历史
     * 
     * @param id 站点历史ID
     * @return 站点历史
     */
    public DatabandSitehistroy selectDatabandSitehistroyById(Long id);

    /**
     * 查询站点历史列表
     * 
     * @param databandSitehistroy 站点历史
     * @return 站点历史集合
     */
    public List<DatabandSitehistroy> selectDatabandSitehistroyList(DatabandSitehistroy databandSitehistroy);

    /**
     * 新增站点历史
     * 
     * @param databandSitehistroy 站点历史
     * @return 结果
     */
    public int insertDatabandSitehistroy(DatabandSitehistroy databandSitehistroy);

    /**
     * 修改站点历史
     * 
     * @param databandSitehistroy 站点历史
     * @return 结果
     */
    public int updateDatabandSitehistroy(DatabandSitehistroy databandSitehistroy);

    /**
     * 删除站点历史
     * 
     * @param id 站点历史ID
     * @return 结果
     */
    public int deleteDatabandSitehistroyById(Long id);

    /**
     * 批量删除站点历史
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandSitehistroyByIds(String[] ids);
}
