package com.ruoyi.web.mapper;

import java.util.List;
import com.ruoyi.web.domain.DatabandReporthistroy;

/**
 * 报版本Mapper接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface DatabandReporthistroyMapper 
{
    /**
     * 查询报版本
     * 
     * @param id 报版本ID
     * @return 报版本
     */
    public DatabandReporthistroy selectDatabandReporthistroyById(Long id);

    /**
     * 查询报版本列表
     * 
     * @param databandReporthistroy 报版本
     * @return 报版本集合
     */
    public List<DatabandReporthistroy> selectDatabandReporthistroyList(DatabandReporthistroy databandReporthistroy);

    /**
     * 新增报版本
     * 
     * @param databandReporthistroy 报版本
     * @return 结果
     */
    public int insertDatabandReporthistroy(DatabandReporthistroy databandReporthistroy);

    /**
     * 修改报版本
     * 
     * @param databandReporthistroy 报版本
     * @return 结果
     */
    public int updateDatabandReporthistroy(DatabandReporthistroy databandReporthistroy);

    /**
     * 删除报版本
     * 
     * @param id 报版本ID
     * @return 结果
     */
    public int deleteDatabandReporthistroyById(Long id);

    /**
     * 批量删除报版本
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandReporthistroyByIds(String[] ids);
}
