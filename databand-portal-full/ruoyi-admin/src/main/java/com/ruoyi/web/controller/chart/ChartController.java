package com.ruoyi.web.controller.chart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.IChartService;

@RestController
@RequestMapping("/chart")
public class ChartController {
     
    @Autowired
    private IChartService chartService;

    @GetMapping("/dydataset")
    public AjaxResult dataset(String datetype,String productline,String channel)
    {
		return AjaxResult.success(chartService.selectDatasetList(datetype, productline, channel));
    }
}
