package org.databandtech.flinkstreaming;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.databandtech.flinkstreaming.Entity.EpgVod;

public class KafkaAggFunction implements AggregateFunction<EpgVod, Long, Long> {

	private static final long serialVersionUID = 1007361403816967685L;
	
	@Override
	public Long createAccumulator() {
		return 0L;
	}
	
	@Override
	public Long add(EpgVod input, Long accumulator) {
		//实际上未用到input，仅仅做了加1处理
		return accumulator + 1;
	}

	@Override
	public Long getResult(Long accumulator) {
		return accumulator;
	}

	@Override
	public Long merge(Long a, Long b) {
		return a+b; //计数器相加，每条记录进来就加
	}



}
