package org.databandtech.streamjob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 本服务的作用：
1、kafka消费业务日志；
2、中间转换清洗，如aggregate、map/flatmap、reduce等操作，对数据转换形变；
3、经过sink管道，流式进入分析数据库；
 * @author Administrator
 *
 */
@SpringBootApplication
public class DatabandStreamjobSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabandStreamjobSpringbootApplication.class, args);
	}

}
