#### databand-mock-api 接口模拟服务

![mock服务](https://gitee.com/475660/databand/raw/master/files/mock1.png "mock服务")

mock服务是面向后端开发、前端测试、APP开发、前端测试人员在真实服务还没完备时，提前模拟接口。
服务端mock过程：

- 1. 通过Netty提供真正mock-http服务端服务（端口自行设置），而不是mockjs的浏览器客户端Js-URL拦截；
- 2. 将request规则和response输出注册进mock服务；
- 3. 拦截request请求，判断请求的分支，比如用户请求路径是/path1,则进入/path2对应的实例，同时也会匹配路径参数或query_string参数规则；
- 4. 返回response，由mock-http服务提供响应数据；
- 5. 或者forward，跳到其他服务url；

## 使用方法  ##
使用方法：

还没有操作录入界面，在数据库中直接注入数据即可（数据提交后，实例大约30秒之内注册到mock服务），数据行对应的是Mock实例。

字段说明：

- id： 自增
- describe：  说明
- method：  请求类型，GET、POST、DELETE
- path：  路径，比如/mypath/{id}/{type}
- path_parameters：  路径参数,因为有可能存在json等格式字符，不便于使用=\,等分隔字符（因为要组装参数数组），所以参数之间使用特殊字符分隔($$$)，比如{id=1},{type=abc}，就使用了{id<=>1}$$${type<=>abc}的表达式。也可以使用正则：{id<=>:[A-Z0-9\\\\-]+}，含义是任意字符。具体可参考数据库里的例子
- query_string_parameters：  当GET类型时的地址栏query_string参数，比如{pid:1},{type:abc}
- req_cookie： cookie集合，map类型，不需要双括号，如{Session:97d43b1e-fe03-4855-926a-f448eddac32f},{..}
- req_headers： head集合，map类型， {Content-Type:text/html;charset=utf-8},{other:othervalue}
- req_jsonbody： 请求body，支持json和string
- resp_statuscode： 默认200
- resp_cookie： 类似req_cookie，map类型
- resp_headers： 类似req_headers，map类型
- resp_body： 响应body，支持json和string，如{
  "id" : 1,
  "name" : "姓名",
  "price" : "123",
  "price2" : "121",
  "enabled" : "true",
  "tags" : [ "tag1", "tag2数组项" ]
}

配置完成后提交数据，进入实例对应的路径测试：如：  
http://localhost:7821/mypath/2/eee  
http://localhost:7821/get  
http://localhost:7821/mypath2?month=10&userid=1223

使用过程：

- 1. 运行sql脚本，在files目录中的mockinstances.sql
- 2. 修改数据库配置项CONNSTR、DBUSER、DBPASSWD，运行App里的main方法，控制台会输出api路径使用提示，ANYCHARS的意思是任意字符数字组合，在程序中使用正则表达式匹配；
- 3. 编译、maven打包(mvn:package)，进入target\目录 ，运行java -jar xxx.jar 
- 4. 开发测试人员使用。

Mock服务启动：

![mock start](https://gitee.com/475660/databand/raw/master/files/mockstart.png "mock start")

各Mock实体属性说明：

![mock属性](https://gitee.com/475660/databand/raw/master/files/mockinfo.png "mock属性")

模拟GET：

![mockget](https://gitee.com/475660/databand/raw/master/files/mockget.png "mock get")

模拟POST：

![mockpost](https://gitee.com/475660/databand/raw/master/files/mockpost.png "mock post ")